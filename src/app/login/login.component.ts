import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {LoginService} from './../services/login.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})


export class LoginComponent implements OnInit {

    loginForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;

    constructor(private route :ActivatedRoute,private router: Router,private loginService:LoginService,private formBuilder: FormBuilder){}
    
    ngOnInit() {

        this.loginForm = this.formBuilder.group({
        username: ['', Validators.required],
        password: ['', Validators.required]
        });
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/home';
    }  
    
    get f() { return this.loginForm.controls; }

    onSubmit() {

        this.submitted = true;

     
        if (this.loginForm.invalid) {
        return;
        }
        
        this.loading = true;
        this.loginService.validateUser(this.f.username.value, this.f.password.value)
        .pipe(first())
        .subscribe(
            (response: any) => {
                if(response =='login successfully'){
                    this.router.navigate([this.returnUrl]);
                    //this.router.navigate(['/temp']);
                  console.log(response);
                 // location.reload()
                }
                
            }, 
                (error: any) => {
                console.log(error)
                }
        );
    }
  
}


