import {Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';
//import { Response } from '@angular/http';
//import { map } from 'rxjs/operators';

@Injectable()
export class LoginService{
     
    constructor(private http:HttpClient){
        console.log('Task Service Initialized...');
    }
    

    validateUser(username: string, password: string) {
        
        return this.http.post(`http://localhost:8080/login`, { username: username, password: password },{responseType: 'text'})
        //.pipe(map((res:Response) => JSON.parse));        
    }
           
}
        

   